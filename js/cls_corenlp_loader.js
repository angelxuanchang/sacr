/*
 *
 * SACR (Script d'Annotation de Chaînes de Référence): a coreference chain
 * annotation tool.
 * 
 * Copyright 2017 Bruno Oberlé.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 * 
 * This program comes with ABSOLUTELY NO WARRANTY.  See the Mozilla Public
 * License, v. 2.0 for more details.
 * 
 * Some questions about the license may have been answered at
 * https://www.mozilla.org/en-US/MPL/2.0/FAQ/.
 * 
 * If you have any question, contact me at boberle.com.
 * 
 * The source code can be found at boberle.com.
 *
 */

var TOKENIZATION_WORD = 1;
var TOKENIZATION_WORD_N_PUNCT = 2;
var TOKENIZATION_CHARACTER = 3;

/**********************************************************************
 *                      CoreNLP Annotation Loader
 *********************************************************************/

class CoreNLPLoader {

   /* preprocessing of the input text: each line = one paragraph, no empty
    * line, etc. NOTE: no need to worry about \r\n because the text is taken
    * from a textarea, which returns only \n
    */
   static normalizeText(text) {
      text = text.replace(/\n(\s+\n)+/g, "\n\n");
      text = text.replace(/\\n\n/g, "\\n");
      text = text.replace(/\n\s*(#[^\n]+)\n/g, "\n\n$1\n\n");
      text = text.replace(/([^\n])[ \t]*\n[ \t]*(?!#|\n+)/g, "$1 ");
      text = text.replace(/[ \t]*\n\n+[ \t]*/g, "\n");
      text = text.replace(/^\n+/g, "");
      text = text.replace(/\n+$/g, "");
      return text;
   }

   constructor(div) {
      this.div = div;
  }

   parse(annotation, options) {
      var options = this.options;
      var parIsHeading = 0;  // 0 = no, 1 = level 1, etc.
      // variables for storing actions for creating links and chains
      var parsedLinks = new Array(); // array of ParsedLink
         // (see the class ParsedLink for details)
      // NOTE: each link found ({name:values text}) is stored in the
      // filoLinks.  When the closing } is encountered, it is popped out from
      // the filoLinks array and stored permanently in the parsedLinks array.
      var filoLinks = new Array(); // array of ParsedLink

      var textTitle = ''; // for the document.title
      var textId = ''; // id

      var paragraph_counter = 1;


      options = options || {};
      // Converts an annotation to Sacr format
      console.log(annotation);
      var colors = CoreNLP.process(annotation, options);
       // Handle document level display
       var par = document.createElement('P');
       if (annotation.docId != null) {
         textId = annotation.docId;
       }
       if (annotation.docDate != null) {
         par.appendChild(document.createTextNode(annotation.docDate));
       }

       var lastParagraph = -1;
       var lastSpeaker;
       var lastAnchor;
       for (var i = 0; i < annotation.sentences.length; i++) {
         var sentence = annotation.sentences[i];
         var currentParagraph = sentence.paragraph;
         if (currentParagraph == undefined) {
           if (sentence.tokens[0].speaker == null || sentence.tokens[0].speaker != lastSpeaker) {
             currentParagraph = lastParagraph+1;
           } else {
             currentParagraph = lastParagraph;
           }
         }
         var startParagraph = currentParagraph !== lastParagraph;
         var comments = [];
         if (startParagraph) {
           comments.push("#turn: " + currentParagraph);
           if (sentence.tokens[0].speaker) {
             comments.push("#speaker: " + sentence.tokens[0].speaker);
           }
         }

         // Handle paragraph header
         if (startParagraph) {
            for (var j = 0; j < comments.length; j++) {
               par = document.createElement('P');
               par.className = CLASS_COMMENT;
               par.appendChild(document.createTextNode(comments[j]));
               this.div.appendChild(par);
            }

            par = document.createElement('P');
            par.className = CLASS_PARAGRAPH;
            if (parIsHeading) {
               par.classList.add(CLASS_HEADING);
               par.classList.add("level"+parIsHeading);
            }
            var par_number = document.createElement('SPAN');
            par_number.className = CLASS_PAR_NUMBER;
            par_number.appendChild(document.createTextNode('[#'+paragraph_counter+'] '));
            paragraph_counter++;
            par.appendChild(par_number);
            parIsHeading = 0;
            this.div.appendChild(par);
         }

         for (var iToken = 0; iToken < sentence.tokens.length; iToken++) {
           var t = sentence.tokens[iToken];
            // append before token symbols
            if (t.before != null) {
              par.appendChild(document.createTextNode(t.before));
           }
           // Handle start mentions
           if (t.startMentions) {
             t.startMentions.sort(function(a, b) {return (b.endIndex-b.startIndex) - (a.endIndex-a.startIndex)});
             for (var j = 0; j < t.startMentions.length; j++) {
               var m = t.startMentions[j];
               var chainName = 'M' + m.corefClusterID;
               var parsedLink = new ParsedLink(chainName);
               parsedLink.properties = m; // dictionary of mention attributes 
               filoLinks.push(parsedLink);
             }
           }

           // Put token
           var anchor = gText.createTokenAnchor(t.originalText);
           anchor.dataset.index = t.index;
           anchor.dataset.sent = sentence.index + 1;
           for (var link of filoLinks) {
             if (!link.startAnchor) {
               link.startAnchor = anchor;
             }
           }
           lastAnchor = anchor;
           par.appendChild(anchor);

           // Handle end mentions
           if (t.endMentions) {
             for (var j = 0; j < t.endMentions.length; j++) {
               filoLinks[filoLinks.length-1].endAnchor = lastAnchor;
               parsedLinks.push(filoLinks.pop());
             }
           }

            // append after token symbols
            if (t.after != null) {
              par.appendChild(document.createTextNode(t.after));
           }
         }
         // sentence end
         par.appendChild(document.createElement('BR'));
         par.normalize();

         lastParagraph = currentParagraph;
         lastSpeaker = sentence.tokens[0].speaker;
       }
      // Copied from cls_sacr_parser

      // set the document title
      try {
         var title = 'SACR';
         if (textId && textTitle) {
            title = "SACR: "+textId+", "+textTitle;
         } else if (textId) {
            title = "SACR: "+textId;
         } else if (textTitle) {
            title = "SACR: "+textTitle;
         }
         document.title = title;
         var doctitle = document.getElementById('doctitle');
         if (doctitle) {
            doctitle.innerHTML = title;
         }
      } catch (err) {
         // buuuuh!
      }

      /* creation of links and chains */
      Link.clearGlobalState();
      for (var parsedLink of parsedLinks) {
         gText.importLink(parsedLink.startAnchor, parsedLink.endAnchor,
            parsedLink.name, parsedLink.properties);
      }

      /* colors */

      //console.log(colors);
      for (var chainName in colors) {
         //console.log(chainName);
         var color = colors[chainName];
         //console.log('color: '+color.string);
         //console.log('exists: '+gText.colorManager.doesThisColorExist(color).toString());
         if (gText.colorManager.doesThisColorExist(color) &&
               gText.colorManager.isThisColorFree(color,
               gText.chainColl.chains)) {
            var chain = gText.chainColl.getChainByName(chainName);
            if (chain && chain.isTrueChain) {
               chain.color = color;
               //console.log('set color:'+color.string);
            }
         }
      }

   }

}

