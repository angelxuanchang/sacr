// Interface to call corenlp server
var DefaultServerAddress = "http://localhost:9000";
var DefaultAnnotators = "tokenize,ssplit,pos,lemma,ner,depparse,natlog,coref";
var DefaultLanguage = 'en';

class CoreNLP {

  static ajax(url, data, callback) {
    var promise = fetch(url, { method: 'POST', body: data });
    if (callback) {
      promise.then(
        (res) =>
          res.json()
            .then((data) => callback(null, data), (err) => callback(err, null)),
        (err) => callback(err, null));
    }
    return promise;
  }

  static makeUrl(serverAddress, params) {
    var keys = Object.keys(params);
    var str = serverAddress + '?';
    for (var i = 0; i < keys.length; i++) {
      var k = keys[i];
      var v = params[k];
        var v2 = typeof v === 'object'? JSON.stringify(v) : v;
      if (i > 0) {
        str += '&';
      }
      str += k + '=' + encodeURIComponent(v2);
    }
    return str;
  }

  static process(annotation, options) {
    options = options || {};
    var colors = {};
    var useCorefChains = annotation.corefs && !options.discardCorefChains;
    if (useCorefChains) {
       var index = 0;
       var clusterIds = Object.keys(annotation.corefs);
       clusterIds.forEach(function(clusterId) {
         var chain = annotation.corefs[clusterId];
         // console.log('got chain', chain);
         for (var i = 0; i < chain.length; ++i) {
           if (options.discardSingletons) {
             if (chain.length == 1) {
               continue; // Skip singletons
             }
           }

           var mention = chain[i];
           mention.corefClusterID = clusterId; // Why this not set
           var tokens = annotation.sentences[mention.sentNum - 1].tokens;
           var iStart = mention.startIndex - 1;
           var iEnd = mention.endIndex - 2;
           if (!tokens[iStart]) {
            console.error("No token for sentence " + (mention.sentNum-1) + ', index ' + iStart, mention);
           }
           if (!tokens[iEnd]) {
            console.error("No token for sentence " + (mention.sentNum-1) + ', index ' + iEnd, mention);
           }
           tokens[iStart].startMentions = tokens[iStart].startMentions || [];
           tokens[iStart].startMentions.push(mention);
           tokens[iEnd].endMentions = tokens[iEnd].endMentions || [];
           tokens[iEnd].endMentions.push(mention);

           colors['M' + clusterId] = gText.getColorManager().getColor(index).string;
           index++;
         }
       });
     }
     return colors;
  }

  static annotationToSacrText(annotation, options) {
    options = options || {};
    // Converts an annotation to Sacr format
    console.log(annotation);
    var colors = CoreNLP.process(annotation, options);
    var text = "";
    var lastParagraph = -1;
    var lastSpeaker;
    for (var i = 0; i < annotation.sentences.length; i++) {
      var sentence = annotation.sentences[i];
      var currentParagraph = sentence.paragraph;
      if (currentParagraph == undefined) {
        if (sentence.tokens[0].speaker == null || sentence.tokens[0].speaker != lastSpeaker) {
          currentParagraph = lastParagraph+1;
        } else {
          currentParagraph = lastParagraph;
        }
      }
      if (currentParagraph !== lastParagraph) {
        if (i > 0) {
          text += "\n";
        }
        text += "#turn: " + currentParagraph + "\n";
        if (sentence.tokens[0].speaker) {
          text += "#speaker: " + sentence.tokens[0].speaker + "\n";
        }
        text += "\n";
      }
      text += sentence.tokens.map(t => {
          var prefix = '';
          var suffix = '';
          if (t.startMentions) {
            t.startMentions.sort(function(a, b) {return (b.endIndex-b.startIndex) - (a.endIndex-a.startIndex)});
            for (var j = 0; j < t.startMentions.length; j++) {
              var m = t.startMentions[j];
              prefix += "{M" + m.corefClusterID + " ";
            }
          }
          if (t.endMentions) {
            for (var j = 0; j < t.endMentions.length; j++) {
              suffix += "}";
            }
          }
          return prefix + t.word + suffix;
        }).join(' ');
      text += "\n";
      lastParagraph = currentParagraph;
      lastSpeaker = sentence.tokens[0].speaker;
    }
    if (colors) {
      var colorKeys = Object.keys(colors);
      colorKeys.forEach(function(colorKey) {
        // Assumes gText.colorManager
        text += "\n#COLOR:" + colorKey + "=" + colors[colorKey];
      });
    }
    text += "\n\n#TOKENIZATION-TYPE:1\n";
    return text;
  }

  /**
   * Get the input date
   */
  static date() {
    function f(n) {
      return n < 10 ? '0' + n : n;
    }
    var date = new Date();
    var M = date.getMonth() + 1;
    var D = date.getDate();
    var Y = date.getFullYear();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    return "" + Y + "-" + f(M) + "-" + f(D) + "T" + f(h) + ':' + f(m) + ':' + f(s);
  }

  constructor(serverAddress, annotators, language) {
    this.serverAddress = serverAddress || DefaultServerAddress;
    this.annotators = annotators || DefaultAnnotators;
    this.language = language || DefaultLanguage;
  }

  fetch(index, callback) {
    var url = CoreNLP.makeUrl(this.serverAddress, {
      properties: this.getProperties(),
      pipelineLanguage: this.language,
      annIndex: index
    });
    return CoreNLP.ajax(url, '', callback);
   }

  process(text, callback) {
    var url = CoreNLP.makeUrl(this.serverAddress, {
      properties: this.getProperties(),
      pipelineLanguage: this.language
    });
    return CoreNLP.ajax(url, encodeURIComponent(text), callback);
  }


  getProperties() {
    return {
      annotators: this.annotators,
      date: CoreNLP.date()
    };
  }
}
