# SACR -- Coreference Chain Annotation Tool

SACR (from the French "Script d'Annotation des Chaînes de Référence") is a tool optimized for coreference chain annotation.

Source code may be found at http://boberle.com.

Documentation can be found in the `user_guide.pdf` file.

Some video tutorials (in French) are available at http://boberle.com/projects/sacr.

The tool is distributed under the terms of the Mozilla Public License v2.  This program comes with ABSOLUTELY NO WARRANTY, see the LICENSE file for more details.


